/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)
*/


#include "utilities.h"
//Used for turnOnABit, turnOffABit, and delay
#include "userInterface.h"

#include "timer_impl.h"
#include "time_ops.h"


//needs to be in extern mode in order to overwrite the 'weak' TIM3_IRQHandler
//see
//http://electronics.stackexchange.com/questions/7660/bug-in-keil-arm-compiler-with-interrupt-handlers-and-c

extern "C" 
{
	void TIM3_IRQHandler(void)
	{
		TIM3->SR &= ~TIM_SR_UIF;
		GPIOB->ODR ^= GPIO_ODR_ODR8;
	}
}


int main()
{
	int8_t returnedValue = ' ';
	char stringBuff[15] = {' '};
	uint8_t stringBuffLocation = 0;
	char command = '0';

	
	portBInit();//Set all the pins of port B to push-pull, 50MHz
	
	USART2Open();
  NVIC_EnableIRQ(TIM3_IRQn); 
	UIConsole();

		
  while(1)
	{
	
		returnedValue = USART2GetByte(' ');
		
		USART2SendByte(returnedValue);
		
		stringBuff[stringBuffLocation] = returnedValue;
		stringBuffLocation++;
		
		//If we recieve a backspace, remove a char, re-write overtop of the old 
		//letter, and then go back a space again.  Don't go back more than 1, if 
		//They hit backspace at the start of the line.
		//We also take care of strings that are too long with this same command.
		if(returnedValue == '\b' || returnedValue == 127 || stringBuffLocation > 13) 
		{
			UIRemoveBackspace(stringBuffLocation);
		}
		if(returnedValue == '\r')
		{
				
			if(stringBuffLocation > 10)
			{
				UIMessageTooLong();
				command = 'H';
			}
			else
			{
				//USART2SendString("In MatchString\n");
				command = UImatchString(stringBuff, stringBuffLocation);
			}
			stringBuffLocation = 0;
			stringBuff[0] = ' ';
			stringBuff[1] = ' ';
			stringBuff[2] = ' ';
			stringBuff[3] = ' ';
			stringBuff[4] = ' ';
			stringBuff[5] = ' ';
			stringBuff[6] = ' ';
			stringBuff[7] = ' ';
			stringBuff[8] = ' ';
			stringBuff[9] = ' ';
			stringBuff[10] = ' ';
			stringBuff[11] = ' ';
			stringBuff[12] = ' ';
			stringBuff[13] = ' ';
			stringBuff[14] = ' ';
			//USART2SendNumber32(stuff);
			UILEDCommand(command);
			UIConsole();
		}
			
	}
	//USART2Close();
  	
}


