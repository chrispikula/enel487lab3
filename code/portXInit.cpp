/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

Description: This is a simple file that just, currently, initializes port B
*/

#include "portXInit.h"

void portBInit(void)
{
	RCC_APB2ENR |= 0x08;
	//RCC->APB2ENR |= 0x08;
	GPIOB_ODR &= 0x0000FF00;
	//GPIOB->ODR &= 0x0000FF00;
	GPIOB_CRH = 0x33333333;
	//GPIOB->CRH = 0x33333333;
	/**
	It doesn't make sense to me to set all of the pins to state 3, but I haven't
	done enough research yet to make sure that *not* setting them to 50MHz is a 
	good idea
	*/
}
