/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

Description: The header file for port initalization
*/


//#include "stm32f10x.h"
//Used for accessing the RCC->APx2ENR, GPIOx->ODR and GPIOx->CRH registers

#include "registers.h"


void portBInit(void);
