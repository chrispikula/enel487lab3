/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

This file is a set of useful registers
*/
typedef unsigned           		int uint32_t;
typedef unsigned short     		int uint16_t;
typedef unsigned          		char uint8_t;

#define PERIPH_BASE           ((uint32_t)0x40000000)

#define APB1PERIPH_BASE       PERIPH_BASE
#define APB2PERIPH_BASE       (PERIPH_BASE + 0x10000)
#define AHBPERIPH_BASE        (PERIPH_BASE + 0x20000)

#define GPIOA_BASE            (APB2PERIPH_BASE + 0x0800)
#define GPIOB_BASE            (APB2PERIPH_BASE + 0x0C00)
#define RCC_BASE              (AHBPERIPH_BASE + 0x1000)
#define USART2_BASE           (APB1PERIPH_BASE + 0x4400)

//Cameron helped me out here, he gave me the trick of the exterior (* ... )
#define GPIOB_ODR							(* 	(volatile uint32_t *)(GPIOB_BASE + 0x0C))
#define GPIOB_CRH  				 		(* 	(volatile uint32_t *)(GPIOB_BASE + 0x04))
#define GPIOB_BSRR 				 		(* 	(volatile uint32_t *)(GPIOB_BASE + 0x10))

#define RCC_APB2ENR  					(*  (volatile uint32_t *)(RCC_BASE + 0x18))	
#define RCC_APB1ENR  					(*  (volatile uint32_t *)(RCC_BASE + 0x1C))

#define GPIOA_ODR  						(*  (volatile uint32_t *)(GPIOA_BASE + 0x0C))
#define GPIOA_CRL  						(*  (volatile uint32_t *)(GPIOA_BASE + 0x00))

#define USART2_BRR  					(* 	(volatile uint32_t *)(USART2_BASE + 0x08))
#define USART2_CR1  					(* 	(volatile uint32_t *)(USART2_BASE + 0x0C))
#define USART2_DR  				 		(* 	(volatile uint32_t *)(USART2_BASE + 0x04))
#define USART2_SR  				 		(* 	(volatile uint32_t *)(USART2_BASE + 0x00))

