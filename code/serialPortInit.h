/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

Description: The header file for serialPortInit(alization)
*/


//#include "stm32f10x.h"
#include "registers.h"

void USART2Init(void);
void USART2Open(void);
void USART2Close(void);
uint8_t USART2SendString(const char*);
uint8_t USART2SendByte(uint8_t);
uint8_t USART2GetByte(uint8_t);
uint8_t USART2SendNumber(uint8_t);
uint32_t USART2SendNumber32(uint32_t);
uint8_t USART2SendDigit(uint8_t);
