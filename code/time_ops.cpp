/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

This file is the file that runs the timing tests
*/

#include "time_ops.h"
#include "timer_impl.h"
#include "utilities.h"
#include "serialPortInit.h"

const uint32_t RUNS = 128;

//These are our three data structures that we use to move data around
struct struct_8_byte{
	 uint32_t tempVar32_1;
   struct_8_byte& operator=( struct_8_byte& a)
{
    tempVar32_1 = a.tempVar32_1;
    return *this;
}
};

struct struct_128_byte{
	 uint32_t tempVar32[16];
		 struct_128_byte& operator=( struct_128_byte& a)
{
	for (int i = 0; i < 32; i++)
	{
    tempVar32[i] = a.tempVar32[i];
	}
    return *this;
}
};
struct struct_1024_byte{
	 uint32_t tempVar32[128];
	 struct_1024_byte& operator=( struct_1024_byte& a)
	{
	for (int i = 0; i < 128; i++)
	{
    tempVar32[i] = a.tempVar32[i];
	}
    return *this;
	}
	
};

//This is our slightly-too-large timing structure.
void time_ops (void)
{
	volatile uint64_t runningTotal = 0;
	uint32_t tempVar32_1 = 0;
	uint32_t tempVar32_2 = 0;
	uint32_t tempVar32_3 = 0;
	uint64_t tempVar64_1 = 0;
	uint64_t tempVar64_2 = 0;
	uint64_t tempVar64_3 = 0;
	uint16_t tempTimer=0;
	uint32_t tempTimerTotal=0;
	volatile uint32_t tempTimerTotalEmpty=0;
	uint32_t randomNumber = 0;
	static struct_8_byte SmallStruct1;
  static struct_128_byte MediumStruct1;
  static struct_1024_byte LargeStruct1;

	static struct_8_byte SmallStruct2;
  static struct_128_byte MediumStruct2;
  static struct_1024_byte LargeStruct2;
	
	//Blank loop
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			randomNumber = randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			tempTimerTotal += timer_stop(tempTimer);
			
			runningTotal += randomNumber;
			//math clear operation goes here
		}
		tempTimerTotalEmpty = tempTimerTotal / RUNS;
		USART2SendNumber32(tempTimerTotalEmpty);
		USART2SendString(" is the number of clock ticks of an empty loop\n\r\0");
	
		//Addition loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			tempVar32_1 = randomInt();
			tempVar32_2 = randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			tempVar32_3 = tempVar32_2 + tempVar32_1;
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			runningTotal += tempVar32_3;
			runningTotal += tempVar32_2;
			runningTotal += tempVar32_1;
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to add two 32 bit numbers\n\r\0");
	
		//64 bit Addition loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			tempVar64_1 = randomInt();
			tempVar64_1 = tempVar64_1 << 32;
			tempVar64_1 += randomInt();
			tempVar64_2 = randomInt();
			tempVar64_2 = tempVar64_2 << 32;
			tempVar64_2 += randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			tempVar64_3 = tempVar64_2 + tempVar64_1;
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			runningTotal += (tempVar64_3);
			runningTotal += (tempVar64_2);
			runningTotal += (tempVar64_1);
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to add two 64 bit numbers\n\r\0");
	
		//Multiplication loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			tempVar32_1 = randomInt();
			tempVar32_2 = randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			tempVar32_3 = tempVar32_2 * tempVar32_1;
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			runningTotal += tempVar32_3;
			runningTotal += tempVar32_1;
			runningTotal += tempVar32_2;
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to multiply two 32 bit numbers\n\r\0");
	
		//64 bit Multiplication loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			tempVar64_1 = randomInt();
			tempVar64_1 = tempVar64_1 << 32;
			tempVar64_1 += randomInt();
			tempVar64_2 = randomInt();
			tempVar64_2 = tempVar64_2 << 32;
			tempVar64_2 += randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			tempVar64_3 = tempVar64_2 * tempVar64_1;
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			runningTotal += (tempVar64_3);
			runningTotal += (tempVar64_2);
			runningTotal += (tempVar64_1);
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to multiply two 64 bit numbers\n\r\0");
	
		//Division loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			tempVar32_1 = randomInt();
			tempVar32_2 = randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			if(tempVar32_1 != 0)
			{
				tempVar32_3 = tempVar32_2 / tempVar32_1;
			}
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			runningTotal += tempVar32_3;
			runningTotal += tempVar32_2;
			runningTotal += tempVar32_1;
			
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to divide two 32 bit numbers\n\r\0");
	
		//64 bit Division loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			tempVar64_1 = randomInt();
			tempVar64_1 = tempVar64_1 << 32;
			tempVar64_1 += randomInt();
			tempVar64_2 = randomInt();
			tempVar64_2 = tempVar64_2 << 32;
			tempVar64_2 += randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			if (tempVar64_1 != 0)
			{
				tempVar64_3 = tempVar64_2 / tempVar64_1;
			}
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			runningTotal += (tempVar64_3);
			runningTotal += (tempVar64_2);
			runningTotal += (tempVar64_1);
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to divide two 64 bit numbers\n\r\0");
	
		//Small Struct loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			SmallStruct1.tempVar32_1 = randomInt();
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			SmallStruct2 = SmallStruct1;
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			SmallStruct1 = SmallStruct2;
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to assign a 8 byte struct\n\r\0");
	
		//Medium Struct loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			for (int i = 0; i < 32; i++)
			{
			MediumStruct1.tempVar32[i] = randomInt();
			}
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			MediumStruct2 = MediumStruct1;
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			MediumStruct1 = MediumStruct2;
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to assign a 128 byte struct\n\r\0");
	
		//Large Struct loop
		tempTimerTotal = 0;
		for (int timeLoop = 0; timeLoop < RUNS; timeLoop++)
		{
			for (int i = 0; i < 128; i++)
			{
			LargeStruct1.tempVar32[i] = randomInt();
			}
			//initalization goes here
			
			tempTimer = timer_start();
			//math operation goes here
			LargeStruct2 = LargeStruct1;
			
			tempTimerTotal += timer_stop(tempTimer);
			
			//math clear operation goes here
			LargeStruct1 = LargeStruct2;
		}
		tempTimerTotal = (tempTimerTotal / RUNS) - tempTimerTotalEmpty;
		USART2SendNumber32(tempTimerTotal);
		USART2SendString(" is the number of clock ticks to assign a 1024 byte struct\n\r\0");
	  USART2SendString("All values have the base time already subtracted.\n\r\0");
		//USART2SendString("Most values are inflated, as I use volatile memory elements \n\r\0");
		//USART2SendString("So the operations require memory accesses. \n\r\0");
	
		//Some junk code here to make sure variables are not optimized away.
		runningTotal += (tempVar32_3);
		runningTotal += (tempVar32_2);
		runningTotal += (tempVar32_1);
		
		runningTotal += tempVar32_2 + tempVar32_3;
		runningTotal += tempVar32_3 + tempVar32_1;
		runningTotal += tempVar64_1 + tempVar64_2;
		//added on the bottom to make sure the variable were not compiled away.
}
