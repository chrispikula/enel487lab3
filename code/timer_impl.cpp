/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

This file is that initializes and closes the timers
*/


#include "timer_impl.h"
#include "stm32f10x.h"
#include "registers.h"
#include "math.h"

//Initialize timer 3 with interrupts
void timer_init_with_interrupts(void)
{
		RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	
	//OPen port A for our LEDS;
	RCC->APB2ENR |= RCC_APB2ENR_IOPAEN;
	GPIOA->ODR &= 0x00;
	GPIOA->CRL &= 0xFFFF00FF;
	GPIOA->CRL |= 0x00008B00;
	
	TIM3->PSC = 1000;
	TIM3->ARR = 47999;
	TIM3->DIER = TIM_DIER_UIE;
	
	//Interrupt Won't Work.
	
	
	TIM3->CR1 = TIM_CR1_CEN;
}

//Initializie timer 2 without interrupts
void TimerInit(void)
{
	//Enable the Timer for our clock
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
	
	
	TIM2->PSC = 0;
	TIM2->ARR = 0xFFFF;
	
	TIM2->CR1 = TIM_CR1_DIR | TIM_CR1_CEN;
}

//return the value of timer 2
int16_t timer_start(void)
{
	return TIM2->CNT;
}

//return the difference of two values of timer 2
int16_t timer_stop(int16_t start_time)
{
	int16_t temp = TIM2->CNT;
	if (temp > start_time)
	{
		return (0xFFFF - start_time + temp) % 0xFFFF;
	}
	else
	{
		return start_time - temp;
	}
}

//Shut down timer 2
void timer_shutdown()
{
	TIM2->CR1 = 0;
	RCC->APB1ENR &= (0xFFFFFFFF - RCC_APB1ENR_TIM2EN);
}

//Shut down timer 3
void timer3_shutdown()
{
	TIM3->CR1 = 0;
	RCC->APB1ENR &= (0xFFFFFFFF - RCC_APB1ENR_TIM3EN);
}

