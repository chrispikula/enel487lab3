/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

This is the header file for the timers
*/


#include "stm32f10x.h"

void TimerInit(void);
void timer_init_with_interrupts(void);
int16_t timer_start(void);
int16_t timer_stop(int16_t start_time);
void timer_shutdown();
void timer3_shutdown();
