/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

This file is the user interface
*/
#include "userInterface.h"
#include "time_ops.h"
#include "timer_impl.h"
#include "ctype.h"
//for int toupper(int c)

static const char * MsgTimeAndDate = __TIME__ " " __DATE__ " \n\r\0";

static const char * CONSOLEMsg = "Console>\0";
static const char * commandHELP = "HELP\r";
static const char * commandLED = "LED ON ";
static const char * commandLEDOFF = "LED OFF ";
static const char * commandStatusLED = "STATUS";
static const char * commandRunTest = "RUN_TEST";
static const char * commandDebug = "DEBUG";
static const char * commandTimer = "TIMER";
static const char * commandTimerOff = "TIMER_OFF";
static const char * ERRORMsg = "Error!\n\r\0";
static const char * ERRORMsg1 = "Invalid Input!\n\r\0";
static const char * ERRORMsg2 =	"Type HELP for a list of correct commands\n\r\0";
static const char * RETURNMsg = "\n\r\0";
static const char * ERRORMsgCommandTooLong = "Command Too Long!\n\r\0";
static const char * ERRORMsgWrongCommandLength = "Command Wrong Length!\n\r\0";
static const char * HELPMsg1 = "To use this program\n\r\0";
static const char * HELPMsg2 = "Type HELP to get this message\n\r\0";
static const char * HELPMsg3 = "Type LED ON x, where x is from 0 to 7\n\r\0";
static const char * HELPMsg4 = "That will turn on a specified LED\n\r\0";
static const char * HELPMsg5 = "Type LED OFF x, where x is from 0 to 7\n\r\0";
static const char * HELPMsg6 = "That will turn off a specified LED\n\r\0";	
static const char * HELPMsg7 = "Typing STATUS will return the status of the LEDs\n\r\0";
static const char * HELPMsg8 = "Typing RUN_TEST will run a series of timing tests\n\r\0";
static const char * HELPMsg9 = "Typing TIMER will run an interrupt timer and blink LED 0\n\r\0";
static const char * HELPMsg10 = "Typing TIMER_OFF will turn off the interrupt timer\n\r\0";
static const char * LEDMsgOn = "You turned on LED \0";
static const char * LEDMsgOff = "You turned off LED \0";
static const char * LEDStatusMsg = "The LEDs that are on are: \n\r\0";

static const char * TIMERStatusMsgOn = "The timer was successfully turned on. \n\r\0";
static const char * TIMERStatusMsgOff = "The timer was successfully turned off. \n\r\0";
void UIConsole(void)
{
	USART2SendString(CONSOLEMsg);
}

void UIMessageTooLong(void)
{	
	USART2SendString(ERRORMsg);
	USART2SendString(RETURNMsg);	
	USART2SendString(ERRORMsgCommandTooLong);
}

char UIRemoveBackspace(uint8_t &stringBuffLocation)
{
	uint8_t returnedValue;
	stringBuffLocation--;
	if(stringBuffLocation >0)
	{
		stringBuffLocation--;
	}
	returnedValue = USART2SendByte(' ');
	returnedValue = USART2SendByte('\b');
	
	if(stringBuffLocation == 0)
	{
		return 0;
	}
	else
	{
		return returnedValue;	
	}
}


char UILEDCommand(char command)
{
	if (command == 'H')
	{
		
		//USART2SendString( );
		USART2SendString(MsgTimeAndDate);
		USART2SendString(HELPMsg1);
		USART2SendString(HELPMsg2);
		USART2SendString(HELPMsg3);
		USART2SendString(HELPMsg4);
		USART2SendString(HELPMsg5);
		USART2SendString(HELPMsg6);
		USART2SendString(HELPMsg7);
		USART2SendString(HELPMsg8);
		USART2SendString(HELPMsg9);
		USART2SendString(HELPMsg10);
	}
	else if (command == 'E')
	{
		USART2SendString(ERRORMsg);
		USART2SendString(ERRORMsg1);
		USART2SendString(ERRORMsg2);
	}
	else if (command == 'R')
	{
		USART2SendString(ERRORMsg);
		USART2SendString(ERRORMsgWrongCommandLength);
		USART2SendString(ERRORMsg2);
	}
	else if (command == 'T')
	{
		USART2SendString("Running Timing Tests:\n\r\0");
		
		TimerInit();
		time_ops();
		timer_shutdown();
	}
	//turn on a bit
	else if (command < 8)
	{
		turnOnABitBSRR(&GPIOB_BSRR, command + 8);	
		USART2SendString(LEDMsgOn);
		USART2SendNumber(command);
		USART2SendString(RETURNMsg);	
	}
	//turn off a bit
	else if (command >= 8 && command < 16)
	{
		turnOffABitBSRR(&GPIOB_BSRR, command);
		USART2SendString(LEDMsgOff);
		USART2SendNumber(command-8);
		USART2SendString(RETURNMsg);	
	}
	else if (command == 'S')
	{
		USART2SendString(LEDStatusMsg);
	}
	else if (command == 'D')
	{
		//timer_init_with_interrupts();
	}
	else if (command == 'I')
	{
		timer_init_with_interrupts();
		USART2SendString(TIMERStatusMsgOn);
	}
	else if (command == 'J')
	{
		timer3_shutdown();
		USART2SendString(TIMERStatusMsgOff);
	}
	return 0;
}

char UImatchString(char* input, int8_t length)
{
	int status=-1;//-1 for start, 0 for mid, +1 for trailing
	int start = 0;
	int end = 0;
	int i = 0;
	char command = '0';
	char LEDVar = '0';
			
	USART2SendByte('\n');
	USART2SendByte('\r');
	
	//Trim leading & trailing whitespace
	for (i = 0; i < length; i++)
	{
		if (status == -1 && input[i] == ' ') //space
		{
			start++;
		}	
		else if (status == 0 && input[i] == ' ') //space
		{
			end++;
		}
		else if (input[i] != ' ' && input[i] != '\r')
		{
			status = 0;
			end = 0;
		}
	}
	//subtract away leading and trailing whitespace
	for (i = 0; i < (length - start - end); i++)
	{
		input[i] = input[i+start];
	}
	length = length - start - end;
	input[length] = '\r';

	
	//Set all lowercase char's to uppercase
	for (i = 0; i< length; i++)
	{
		if (input[i] > 96 && input[i] < 123)
		{
			input[i] = input[i] - 32;
		}
	}
	
	
		
		

	//Letters USED:
	//DEFHORSTIJ
	//Refactor to use static constants by next lab.
	
	//Match 'help'
	for(i = 0; i < length-1; i++)
	{
		if (length == 5)
		{
			
			if (input[i] == commandHELP[i] && command != 'E')
			{
				command = 'H';
			}
			else
			{
				command = 'E';
				break;
			}
		}
		else if (length == 6)
		{		
			if (input[i] == commandDebug[i] && command != 'E' && command != 'I')
			{
				command = 'D';
				if (i == 5)
				{
					break;
				}
			}
			else if(input[i] == commandTimer[i] && command !='E' && command != 'D')
			{
				command = 'I';
				if (i == 5)
				{
					break;
				}
			}
			else
			{
				command = 'E';
				break;
			}
		}
		else if (length == 7)
		{
			if (input[i] == commandStatusLED[i] && command != 'E')
			{
				command = 'S';
				if (i == 6)
				{
					break;
				}
			}
			else
			{
				command = 'E';
				break;
			}
		}
		else if (length == 9)
		{
			if (input[i] == commandLED[i] && command != 'E' && command != 'R')
			{
				command = 'O';			
				if (i == 6)
				{
					break;
				}
			}
			else if (input[i] == commandRunTest[i] && command != 'E' && command != 'O')
			{
				command = 'T';
				if (i == 7)
				{
					break;
				}
			}
			
			else
			{
				command = 'E';
				break;
			}
		}
		else if (length == 10)
		{
			if (input[i] == commandLEDOFF[i] && command != 'E' && command != 'J')
			{
				command = 'F';
				if (i == 7)
				{
					break;
				}
			}
			else if (input[i] == commandTimerOff[i] && command != 'E' && command != 'F')
			{
				command = 'J';
				if (i == 7)
				{
					break;
				}
			}
			else
			{
				command = 'E';
				break;
			}
		}
		else
		{
			command = 'R';
			break;
		}
	}
	if (command == 'O' || command == 'F')
	{
			LEDVar = input[i+1];

			if (LEDVar == '0')
			{
				if (command == 'O')
				{
					command = 0;
				}
				else
				{
					command = 8;
				}
			}
			else if (LEDVar == '1')
			{
				if (command == 'O')
				{
					command = 1;
				}
				else
				{
					command = 9;
				}
			}
			else if (LEDVar == '2')
			{
				if (command == 'O')
				{
					command = 2;
				}
				else
				{
					command = 10;
				}
			}
			else if (LEDVar == '3')
			{
				if (command == 'O')
				{
					command = 3;
				}
				else
				{
					command = 11;
				}
			}
			else if (LEDVar == '4')
			{
				if (command == 'O')
				{
					command = 4;
				}
				else
				{
					command = 12;
				}
			}
			else if (LEDVar == '5')
			{
				if (command == 'O')
				{
					command = 5;
				}
				else
				{
					command = 13;
				}
			}
			else if (LEDVar == '6')
			{
				if (command == 'O')
				{
					command = 6;
				}
				else
				{
					command = 14;
				}
			}
			else if (LEDVar == '7')
			{
				if (command == 'O')
				{
					command = 7;
				}
				else
				{
					command = 15;
				}
			}
			else
			{
				command = 'E';
			}
	}
	if (length == 1)
	{
		command = 'R';
	}
	return command;
}
