/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

This file is header file for the user interface
*/

#include "portXInit.h"
//Used to for portBInit
#include "serialPortInit.h"
//Used for USART2Init
#include "utilities.h"
//For variable initialization

char UImatchString(char* input, int8_t length);
char UILEDCommand(char command);
char UIRemoveBackspace(uint8_t&);
void UIMessageTooLong(void);
void UIConsole();
