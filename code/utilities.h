/**
Programmer: Chris Pikula
Project: Timing Tests
Date: 2016-10-18

Description: This is part of a program that turns on and off
LEDs via communicating to and from the console.
It can also run some timing tests, as well as use an interrupt timer
	(Via timers 2 and 3, respectively)

Description: The header file for utility functions.
*/

#include "stm32f10x.h"
//Used for 'uint32_t', 'uint8_t' 

void delay(uint32_t delay);
void turnOnABitBSRR(volatile uint32_t * value, uint8_t bit);
void turnOffABitBSRR(volatile uint32_t * value, uint8_t bit);
uint32_t randomInt(void);
